import os
import sys
import argparse
import glob
import zipfile

try:
    import rarfile
    RAR_INSTALLED = True
except:
    RAR_INSTALLED = False

import mosspy


def parse_args():
    parser = argparse.ArgumentParser(description="Check student solutions using MOSS system.")
    parser.add_argument("--folder", required=True, type=str, help="main folder with solutions")
    parser.add_argument("--moss_userid", type=int, default=893813850, help="User ID to for MOSS")
    parser.add_argument("--extra_folders", nargs ="+", default=[], help="Extra folders to compare against")
    parser.add_argument("--official_code", nargs ="+", default=[], help="Folders with code that was allowed to use")
    args = parser.parse_args()
    return args


def parse_archive(archive, file_extensions_to_merge):
    content = archive.namelist()
    list_of_lines = []
    for file_name in content:
        is_correct_ext = any(file_name.lower().endswith(ext) for ext in file_extensions_to_merge)
        if is_correct_ext and len(file_name) > 0 and not file_name[0] in ['.', '~']: # adding some security checks for archive containing files outside of them
            try:
                test_file = archive.read(file_name)
                if test_file:
                    test_file = test_file.decode('utf-8')
                    lines = test_file.splitlines(False)
                    if not list_of_lines:
                        list_of_lines = lines
                    else:
                        list_of_lines = list_of_lines + [""] * 2 + lines
            except:
                print(f"Could not parse {file_name}")
    return list_of_lines


def extract_files_zip_archive(file_path, file_extensions_to_merge=[".py"]):
    try:
        read_files = []
        with  zipfile.ZipFile(file_path) as z:
            exit_code = z.testzip()

            if exit_code is not None:
                return None

            read_files = parse_archive(z, file_extensions_to_merge)

        return read_files
    except:
        return None


def extract_files_rar_archive(file_path, file_extensions_to_merge=[".py"]):
    try:
        if not RAR_INSTALLED:
            print('rarfile module is not installed: cannot unpack', file_path)
            return None

        read_files = []
        with  rarfile.RarFile(file_path) as rf:
            exit_code = rf.testrar()

            if exit_code is not None:
                return None

            read_files = parse_archive(rf, file_extensions_to_merge)

        return read_files
    except:
        return None


def create_solution_single_file(solution_path, output_file_name,
                                file_extensions_to_merge=[".py"],
                                target_extension=".py"):
    print(f"Creating solution file {output_file_name} in path {solution_path}")
    list_of_lines = []

    for fname in glob.iglob(solution_path + "/*"):
        print(fname)
        lines = []

        if len(fname) >= 4 and fname[-4:].lower() == '.zip':
            # found a ZIP archive, need to unpack
            lines = extract_files_zip_archive(fname,
                                              file_extensions_to_merge=file_extensions_to_merge)
        elif RAR_INSTALLED and len(fname) >= 4 and fname[-4:].lower() == '.rar':
            # found a RAR archive, need to unpack
            lines = extract_files_rar_archive(fname,
                                              file_extensions_to_merge=file_extensions_to_merge)
        elif len(fname) >= 4:
            is_correct_ext = any(fname.endswith(ext) for ext in file_extensions_to_merge)
            if is_correct_ext:
                with open(fname, "r") as f:
                    lines = f.read().splitlines(False)

        if not list_of_lines:
            list_of_lines = lines
        else:
            list_of_lines = list_of_lines + [""] * 2 + lines

        with open(output_file_name, 'w') as f:
            for l in list_of_lines:
                f.write(f"{l}\n")


def add_solution_folder(path_solutions, moss_instance):
    # loop over solutions
    for solution_path in glob.iglob(path_solutions + "/*"):
        subfolder_name = os.path.basename(os.path.normpath(solution_path))
        if "report" in subfolder_name:
            continue
        merged_student_file = os.path.join(solution_path,
                                           subfolder_name + ".py")

        if not os.path.isfile(merged_student_file):
            create_solution_single_file(solution_path, merged_student_file)

        moss_instance.addFile(merged_student_file)


def add_base_code_folder(path, moss_instance):
    merged_code_file = os.path.join(path,
                                    os.path.basename(os.path.normpath(path)) + ".py")

    if not os.path.isfile(merged_code_file):
        create_solution_single_file(path, merged_code_file)

        moss_instance.addBaseFile(merged_code_file)


def main(args):
    path_solutions = args.folder
    moss_instance = mosspy.Moss(args.moss_userid, "python")

    # main folder with solutions
    add_solution_folder(path_solutions, moss_instance)

    # add extra folders if available
    for extra_folder in args.extra_folders:
        add_solution_folder(extra_folder, moss_instance)

    # add base folders - code that was allowed to use
    for folder in args.official_code:
        add_base_code_folder(folder, moss_instance)

    url = moss_instance.send() # Submission Report URL
    print ("Report Url: " + url)

    # Save report file
    moss_instance.saveWebPage(url, os.path.join(path_solutions, "report.html"))

    # Download whole report locally including code diff links
    mosspy.download_report(url, os.path.join(path_solutions, "report"), connections=8)


if __name__ == '__main__':
    args = parse_args()
    main(args)
