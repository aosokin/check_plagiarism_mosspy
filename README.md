# Script to check if the Python code is copied

Script uses [A Python client](https://github.com/soachishti/moss.py) for [Moss](http://theory.stanford.edu/~aiken/moss/): A System for Detecting Software Similarity

## Installation
```shell
pip install mosspy
```

## Usage
* This code by default uses the Moss ID of Anton Osokin: `moss_userid = 893813850`. If you plan to use it extensively, please, [get your own](https://theory.stanford.edu/~aiken/moss/) - it takes one e-mail (the reply is automatic and immediate).
* Moss was not available from Russia (or at least from HSE). Please, used VPN.
* We assume that all the solutions are stored in the folder `PATH_TO_SOLUTIONS`. Each individual solution lies in a subfolder called with a student surname. Inside, we can have a ZIP archive with no internal archives, or python files directly.
```
PATH_TO_SOLUTIONS:
    Student1:
        code.zip
        main.py
    Student2:
        code.zip
    Student3:
        main1.py
        main2.py
        main3.py
    CODE_FROM_GITHUB:
        main.zip
```
* Launch
```shell
python check_folder.py PATH_TO_SOLUTIONS
```
and get web page PATH_TO_SOLUTIONS/report.html with its data in PATH_TO_SOLUTIONS/report.

## Python Compatibility

* [Python](http://www.python.com) - v3.7

## License

This project is licensed under the MIT License